$(document).ready(function () {
  $(window).scroll(function () {
    if (this.scrollY > 0) {
      $(".navbar").addClass("sticky");
    } else {
      $(".navbar").removeClass("sticky");
    }
  });

  // toggle menu in navbar

  $(".menu-button").click(function () {
    $(".navbar .menu").toggleClass("active");
    $(".menu-button i").toggleClass("active");
  });
});

// typing animation typed.js

var typed = new Typed(".typing", {
  strings: ["Student.", "Developer.", "Freelancer."],
  typespeed: 80,
  backspeed: 60,
  loop: true,
});
